import functools
from datetime import datetime
from discord.ext import commands
import psutil

# wrapper function,
# make sure bot will only reply when someone really wishes to use them
def direct_command(f):
    def predicate(ctx):
        return ctx.bot.user in ctx.message.mentions or ctx.guild is None

    @functools.wraps(f)
    async def wrapped(self, ctx, *args, **kwargs):
        if predicate(ctx):
            return await f(self, ctx, *args, **kwargs)
        return None

    return wrapped

class Info(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def info(self, ctx):
        pass

    @info.command()
    @direct_command
    async def owner(self, ctx):
        info = await self.bot.application_info()
        await ctx.send(info.owner)

    @info.command()
    @direct_command
    async def uptime(self, ctx):
        proc = psutil.Process()
        ctime = proc.create_time()
        u = datetime.now() - datetime.fromtimestamp(ctime)
        await ctx.send("Uptime: {}".format(str(u)))

def setup(bot):
    bot.add_cog(Info(bot))

def cleanup(bot):
    bot.remove_cog(Info.qualified_name)
