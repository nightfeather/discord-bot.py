from discord.ext import commands

class Owner(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    @commands.is_owner()
    @commands.dm_only()
    async def maintain(self, ctx):
        pass

    @maintain.command()
    async def exts(self, ctx):
        for k in self.bot.extensions:
            await ctx.send(k)

    @maintain.command()
    async def load(self, ctx, arg):
        try:
            self.bot.load_extension(arg)
            await ctx.send("Extension `{}` loaded".format(arg))
        except commands.ExtensionError as e:
            await ctx.send("Error occurred while loading extension: {}".format(arg))
            await ctx.send("Reason: {}".format(e.name))
        except Exception as e:
            await ctx.send("Unexpected error occurred while loading extension: {}".format(arg))
            await ctx.send("Reason: {}".format(str(e)))

    @maintain.command()
    async def reload(self, ctx, *args):
        if args[0] == 'all':
            args = self.bot.extensions.keys()

        async with ctx.typing():
            error_count = 0
            for arg in args:
                if arg in self.bot.extensions:
                    try:
                        await ctx.send('Reloading {}'.format(arg))
                        self.bot.reload_extension(arg)
                    except commands.ExtensionError as e:
                        error_count += 1
                        await ctx.send('Extension {} failed to reload'.format(arg))
                        await ctx.send('Reason: {}'.format(e.__qualname__))
                    except Exception as e:
                        error_count += 1
                        await ctx.send('Unexpected error occurred.')
                else:
                    await ctx.send('invalid extension name')
            await ctx.send('Reloaded {} extensions, with {} error(s).'.format(len(args), error_count))

def setup(bot):
    bot.add_cog(Owner(bot))

def cleanup(bot):
    bot.remove_cog(Owner.qualified_name)
