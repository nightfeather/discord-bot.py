from functools import total_ordering

@total_ordering
class Boss:
    def __init__(self, iteration, boss, limit):
        self.iteration = iteration
        self.boss = boss
        self.limit = limit

    def is_valid(self):
        return self.boss in range(1, self.limit+1)

    def __add__(self, value):
        i, b = self.iteration, self.boss
        b += value
        if b > self.limit:
            i += b // self.limit
            b = b % self.limit

        return Boss(i, b, self.limit)

    def __iter__(self):
        return iter((self.iteration, self.boss))

    def __lt__(self, other):
        return tuple(self) < tuple(other)

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __repr__(self):
        return "Boss({},{})".format(self.iteration, self.boss)
