from typing import Dict, Union
from ..model import base
from ..misc import Boss

class MetadataMixin():

    def __init__(self):
        self.tables: Dict[str, base.GSModelBase, base.GSAttrs]

    @property
    def paused(self):
        return self.tables['metadata']['paused']

    @paused.setter
    def paused(self, value):
        self.tables['metadata']['paused'] = value

    @property
    def current_boss(self):
        it = int(self.tables['metadata']['iteration'])
        bi = int(self.tables['metadata']['boss_index'])
        ab = int(self.tables['metadata']['boss_count'])
        return Boss(it, bi, ab)

    @current_boss.setter
    def current_boss(self, boss: Boss):
        it, bi = tuple(boss)
        self.tables['metadata']['iteration'] = it
        self.tables['metadata']['boss_index'] = bi

    @property
    def state(self):
        return self.tables['metadata']['state']

    @state.setter
    def state(self, value):
        self.tables['metadata']['state'] = value

    @property
    def boss_count(self):
        return int(self.tables['metadata']['boss_count'])
