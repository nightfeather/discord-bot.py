from typing import Dict, Union
from ..model import base
from ..misc import Boss

class MemberMixin():

    def __init__(self):
        self.tables: Dict[str, base.GSModelBase, base.GSAttrs]

    def add_member(self, discord_id):
        return self['members'].add(discord_id)

    def get_member(self, discord_id):
        return self['members'].get_user(discord_id)
