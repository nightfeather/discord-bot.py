# pylint: disable=unsubscriptable-object
from __future__ import annotations
from datetime import datetime, timedelta
import os
from typing import Dict, Type, Union

import gspread
from google.oauth2 import service_account
from ..model import (
    base,
    Metadata,
    Members,
    AttackHistory,
    BossSettings,
    DamageReports,
    WaitingQueue
)
from .metadata_mixin import MetadataMixin
from .boss_mixin import BossMixin
from .member_mixin import MemberMixin

class GuildRecordMeta(type):

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cls.instances = {}

    def bind(cls, guild, key):
        creds_path = os.environ.get('GOOGLE_CREDENTIAL_FILE', 'creds.json')
        creds = service_account.Credentials.from_service_account_file(creds_path)
        scoped = creds.with_scopes([
            'https://www.googleapis.com/auth/drive',
            'https://www.googleapis.com/auth/drive.file',
            'https://www.googleapis.com/auth/spreadsheets'
        ])
        client = gspread.authorize(scoped)

        sheets = client.open_by_url(key)
        o = GuildRecord(sheets)
        o.populate()
        cls.instances[guild] = o
        return o

    def __setitem__(cls, guild, value):
        cls.instances[guild] = value

    def __getitem__(cls, guild):
        return cls.instances[guild]

    def __contains__(cls, guild):
        return guild in cls.instances

class GuildRecord(MetadataMixin, BossMixin, MemberMixin, metaclass=GuildRecordMeta):
    TABLES: Dict[str, Union[Type[base.GSModelBase],Type[base.GSAttrs]]] = {
        'metadata': Metadata,
        'members': Members,

        'attack_history': AttackHistory,
        'boss_settings': BossSettings,
        'damage_reports': DamageReports,
        'waiting': WaitingQueue
    }

    __slots__ = ['sheets', 'tables']

    def __init__(self, sheets):
        self.sheets = sheets
        self.tables = {}

    # Create sheets if missing
    def populate(self):
        exists = [worksheet.title for worksheet in self.sheets.worksheets()]
        for title in self.TABLES:
            if title not in exists:
                self.sheets.add_worksheet(title, 100, 100)

    # Initialize sheets
    def setup(self):
        for title in self.TABLES:
            self.sheets.worksheet(title).clear()
            self.tables[title] = self.TABLES[title](
                self.sheets.worksheet(title),
                'A1'
            )
            self.tables[title].setup()

    # Read sheets and preload data into cache
    def load(self):
        for title in self.TABLES:
            self.tables[title] = self.TABLES[title](
                self.sheets.worksheet(title),
                'A1'
            )
            self.tables[title].load()

    # Selective flush caches
    def flush(self, model, parts='both'):
        if model == 'all':
            for k in GuildRecord.TABLES:
                self[k].flush(parts)
        elif model in GuildRecord.TABLES:
            self[model].flush(parts)
        else:
            print('wtf? flushing nonexistant model: {}'.format(model))

    # Selective clear local cache and force resync
    def reload(self, model):
        if model == 'all':
            for t in GuildRecord.TABLES:
                self[t].reload()
        elif model in GuildRecord.TABLES:
            self[model].reload()
        else:
            print('wtf? reloading nonexistant model: {}'.format(model))

    # Validate table state
    def validate(self):
        pass

    def enqueue(self, discord_id, iteration, bossi, is_residue=False):
        return self['waiting'].add(discord_id, iteration, bossi, is_residue=is_residue)

    def checkInQueue(self, discord_id, iteration, bossi):
        ids = [rec[0] for rec in self['waiting'].waiting_for(iteration, bossi)]
        return str(discord_id) in ids

    # report to current boss
    def report(self, discord_id, damage):
        i, bi = self.current_boss
        recs = [
            rec
            for rec in self['damage_reports'].reported_for(i, bi) if rec[0] == str(discord_id)
        ]

        if len(recs) > 0:
            return False

        return self['damage_reports'].add(discord_id, i, bi, damage)

    # check enough waitings to next boss
    def queue_full_for(self, iteration, bossi):
        limit = int(self['boss_settings'].get(bossi, 'limit'))
        return len(self['waiting'].waiting_for(iteration, bossi)) >= limit

    def current_reported(self):
        i, bi = self.current_boss
        return self['damage_reports'].reported_for(i, bi)

    def all_reported(self):
        i, bi = self.current_boss
        wids = self.waiting_for(i, bi)
        recs = self.current_reported()
        return len(recs) >= len(wids)

    def pending_boss(self):
        return self.current_boss + 1

    def next_boss(self):
        self.current_boss += 1

    def waiting_for(self, i, bi, did=None):
        return self['waiting'].waiting_for(i, bi, did)

    def current_waiting(self):
        i, bi = self.pending_boss()
        return self['waiting'].waiting_for(i, bi)

    # finish him!
    def compute_damage(self):
        i, bi = self.current_boss
        hp = int(self['boss_settings'].get(bi, 'hp'))
        recs = self.current_reported()
        waits = self.waiting_for(i, bi)
        preselected = [rec[0] for rec in waits if rec[-2] == '1']
        selected = [rec[0] for rec in waits if rec[-2] == '1']
        candidate = [rec for rec in recs if not rec[0] in selected]
        cumulated = sum([int(rec[-2]) for rec in recs if rec[0] in selected])

        for rec in list(sorted(candidate, key=lambda r: r[-2], reverse=True)):
            selected.append(rec[0])
            cumulated += int(rec[-2])
            if cumulated >= hp:
                break

        return (preselected, selected, cumulated, hp)

    def write_history(self, iteration, bossi, selected):
        waits = self.waiting_for(iteration, bossi)
        ct = datetime.now()
        for wait in waits:
            if wait[0] in selected:
                waited: timedelta = ct - datetime.fromisoformat(wait[-1])
                self['attack_history'].add(wait[0], iteration, bossi, waited.total_seconds())

    def __getitem__(self, arg):
        return self.tables[arg]

    def __contains__(self, arg):
        return arg in self.tables
