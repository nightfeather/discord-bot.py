from typing import Dict, Union
from ..model import base
from ..misc import Boss

class BossMixin():

    def __init__(self):
        self.tables: Dict[str, base.GSModelBase, base.GSAttrs]

    def get_boss(self, boss: int, attr=None):
        return self.tables['boss_settings'].get(boss, attr)
