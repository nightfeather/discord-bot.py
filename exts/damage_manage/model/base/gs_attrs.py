from typing import List
import gspread
from .gs_cell_cache import GSCellCache
from ..utils import helpers

class GSAttrs():
    """
    General key-value pair storage
    """

    __slots__ = ['sheet', 'fields', 'pivot', 'cache']
    _fields = [] # type: List[str]

    def __new__(cls, *args, **kwargs):
        if not cls._fields:
            raise NotImplementedError("subclass should override `_fields` class variable")
        obj = super(GSAttrs, cls).__new__(cls)
        obj.__init__(*args, **kwargs)
        return obj

    def __init__(self, sheet, pivot, *, cache=None):
        self.sheet: gspread.Worksheet = sheet
        self.fields = self._fields
        self.pivot = pivot
        self.cache = GSCellCache() if cache is None else cache

    def setup(self):
        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        for off in range(len(self.fields)):
            self.cache[(row+off, col)] = gspread.Cell(row+off, col, self.fields[off])
        return True

    def load(self):
        self.prefetch()
        return True

    def prefetch(self):
        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        datarange = helpers.grid_to_a1_range(row, col, row+len(self.fields)-1, col+1)
        res = self.sheet.range(datarange)
        self.cache.preload(res)

    def get(self, key, *, width=1, force=False):
        if not key in self.fields:
            raise KeyError(key)

        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        off = self.fields.index(key)

        datarange = helpers.grid_to_a1_range(row+off, col+1, row+off, col+width)
        if datarange not in self.cache or force:
            res = self.sheet.range(datarange)
            for cell in res:
                self.cache[(cell.row, cell.col)] = cell

        return [cell.value for cell in self.cache[datarange]]

    def set(self, key, *value):
        if not key in self.fields:
            raise KeyError(key)

        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        off = self.fields.index(key)
        width = len(value)
        datarange = helpers.grid_to_a1_range(
            row+off, col+1,
            row+off, col+width
        )

        for idx in range(width):
            self.cache[(row+off, col+idx+1)] = gspread.Cell(row+off, col+idx+1, str(value[idx]))

    def __setitem__(self, key, value):
        return self.set(key, value)

    def __getitem__(self, key):
        return self.get(key)[0]

    def __len__(self):
        return len(self.fields)

    def flush(self, *_): # eats extranous arguments
        if self.synced():
            return None

        res = self.sheet.update_cells(self.changes())
        self.cache.clear_changes()
        return res

    def changes(self):
        return self.cache.changes()

    def synced(self):
        return len(self.changes()) <= 0

    def reload(self):
        self.cache.flush()
        self.prefetch()
