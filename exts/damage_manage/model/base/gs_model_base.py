from typing import List, Union, Tuple, Any
import gspread
from .gs_cell_cache import GSCellCache
from .gs_attrs import GSAttrs
from ..utils import helpers

class GSModelAttrs(GSAttrs):
    """
    Metadata dedicated to GSModelBase
    """

    _fields = ['modelType', 'columns', 'pivot', 'count', 'size', 'vacancies']

    def setup(self):
        """
        Initialize all fields, modelType is useless, get rid of it or make use of it later.
        """
        super().setup()
        self['columns'] = ''
        self['modelType'] = 'base'
        self['pivot'] = self.pivot
        self['size'] = 0
        self['count'] = 0
        self['vacancies'] = ''

class GSModelBase:
    """
    Class to wrap operations to Google Spreadsheet,
    make it a bit more like a database table,
    so please have table operation in mind when using this :P

    And since there's no api allows operation for cell deletion,
    we'll just blank it and mark them as vacancy.
    """
    __slots__ = ['_metadata', 'sheet', 'pointer', 'cache']
    _metadata_class = GSModelAttrs
    _columns: List[str] = ['']

    def __new__(cls, *args, **kwargs):
        if not cls._columns:
            raise NotImplementedError("subclass should override `_columns` class variable")
        obj = super(GSModelBase, cls).__new__(cls)
        obj.__init__(*args, **kwargs)
        return obj

    def __init__(self, sheet: gspread.Worksheet, pivot: str, metadata=None):
        self.sheet: gspread.Worksheet = sheet
        self.cache = GSCellCache()
        self.pointer = pivot
        self._metadata = self._metadata_class(sheet, pivot) if not metadata else metadata

    def setup(self):
        """
        Do table initialization on Google Sheets
        """
        self._metadata.setup()
        self._metadata['columns'] = ','.join(self._columns)
        self.pivot = self.pointer
        self.next(len(self._metadata))
        self._write_record(self.columns, pivot=self.pointer)
        self.next()
        self.size = 0
        self.count = 0
        self.pivot = self.pointer

    def load(self):
        """
        Load metadata and all rows from Google Sheets,
        maybe we should leave whether prefetch or not to subclass?
        """

        self._metadata.load()
        self.pointer = self.pivot
        self.prefetch(self.size)

# Method group: metadata proxies
    @property
    def pivot(self):
        return self._metadata['pivot']

    @pivot.setter
    def pivot(self, value):
        self._metadata['pivot'] = value

    @property
    def size(self):
        return int(self._metadata['size'])

    @size.setter
    def size(self, value):
        self._metadata['size'] = value

    @property
    def count(self):
        return int(self._metadata['count'])

    @count.setter
    def count(self, value):
        self._metadata['count'] = value

    @property
    def vacancies(self):
        return self._metadata['vacancies'].split(',') if self._metadata['vacancies'] else []

    @vacancies.setter
    def vacancies(self, value):
        self._metadata['vacancies'] = ','.join(sorted(value))

    @property
    def columns(self):
        return self._metadata['columns'].split(',')

    @property
    def modelType(self):
        return self._metadata['modelType']

# Method group end

# Method group: pointer manipulation

    def first_valid_slot(self):
        """
            Return first anchor for row that eligble for new record,
            will query vacancies first, then just move to last row
        """
        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        slot = gspread.utils.rowcol_to_a1(row+self.size, col)
        if len(self.vacancies) > 0:
            slot = self.vacancies[0]
            self.vacancies = self.vacancies[1:]
        return slot

    def next(self, count=1):
        """
            Move pointer to next row
        """
        self.pointer = helpers.a1_offset(self.pointer, count)
        return self.pointer

    def rewind(self):
        """
            Reset pointer back to first row
        """
        self.pointer = self.pivot

# Method group end

# Method group: record manipulation - internals

    def _write_record(self, record: Union[List, Tuple], *, pivot: str):
        row, col = gspread.utils.a1_to_rowcol(pivot)
        width = len(self.columns)
        res = []
        for off in range(width):
            cell = gspread.Cell(row, col+off, str(record[off]))
            self.cache[(row, col+off)] = cell
            res.append(cell)

        return res

# Method group end

# Method group: record manipulation

    def add(self, record: Union[List, Tuple]):
        """ write to first valid slots (first vacancy row or just append) """

        pivot = self.first_valid_slot()
        res = self._write_record(record, pivot=pivot)
        if res:
            self.count += 1
            self.size = max(self.count, self.size)
        return res

    def find_all(self, criteria: List[Tuple[str, Any]]):
        """
            find for rows match supplied criterias,
            will apply criteria by order supplied.
        """
        rows = list(range(self.size))
        for rule in criteria:
            res = self.read_col(rule[0])
            if not res:
                print('rule cannot match anything: @{} ({},{})'.format(self.sheet.title, *rule))
                break
            if hasattr(rule[1], '__call__'):
                rows = [row for row in rows if rule[1](res[row])]
            else:
                rows = [row for row in rows if res[row] == rule[1]]
            if not rows:
                break

        return rows

    def replace(self, idx: int, record: Union[List, Tuple]):
        """
            replace spcified row
        """
        if idx > self.size:
            return None
        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        pivot = gspread.utils.rowcol_to_a1(row+idx, col)
        return self._write_record(record, pivot=pivot)

    def update(self, idx: int, column: str, value):
        """
            update specified column of row index supplied
        """
        if idx > self.size or not column in self.columns:
            return None
        record = self.get_row(idx)
        off = self.columns.index(column)
        record[off] = value
        return self.replace(idx, record)

    def remove_row(self, idx):
        """
            clear specified row,
            and mark this row eligble to overwritten
        """
        pivot = helpers.a1_offset(self.pivot, idx)

        if pivot in self.vacancies:
            return None

        rec = self.get_row(idx)
        self.replace(idx, ['']*len(self.columns))
        self.vacancies = self.vacancies+[pivot]
        self.count -= 1

        return rec

    def get_row(self, idx: Union[int, str]):
        """
            Get row specified by index
        """
        if idx >= self.size:
            return None
        pivot = None
        if isinstance(idx, int):
            pivot = helpers.a1_offset(self.pivot, idx)
        elif isinstance(idx, str):
            pivot = idx

        return self.read_rows(1, pivot=pivot)[0]

    def read_rows(self, count: int, *, pivot=None):
        """
            Read contagious rows
        """
        _pivot = pivot if pivot else self.pivot
        row, col = gspread.utils.a1_to_rowcol(_pivot)
        datarange = helpers.grid_to_a1_range(row, col, row+count-1, col+len(self.columns)-1)

        if datarange not in self.cache:
            res = self.sheet.range(datarange)
            self.cache.preload(res)

        return [
            [self.cache[(row+r, col+c)].value for c in range(len(self.columns))]
            for r in range(count)
        ]

    def read_col(self, column: Union[str, int]):
        """
            Read a column of all rows
        """
        if isinstance(column, str) and column not in self.columns:
            return None
        if isinstance(column, int) and column > len(self.columns):
            return None

        idx = column if isinstance(column, int) else self.columns.index(column)

        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        datarange = helpers.grid_to_a1_range(row, col+idx, row+self.size-1, col+idx)

        if datarange not in self.cache:
            res = self.sheet.range(datarange)
            self.cache.preload(res)

        return [cell.value for cell in self.cache[datarange]]

# Method group end

# Method group: cache manipulation

    def changes(self):
        """ Pendings changes """
        return self.cache.changes()

    def prefetch(self, rows: int):
        """ Read first n rows and load into cache """
        row, col = gspread.utils.a1_to_rowcol(self.pivot)
        datarange = helpers.grid_to_a1_range(row, col, row+rows-1, col+len(self.columns)-1)
        data = self.sheet.range(datarange)
        self.cache.preload(data)

    def synced(self):
        """ Whether have pending changes or not """
        return len(self.changes()) <= 0

    def flush(self, part='both'):
        """ Write changes back to Google Sheets """

        if part == 'data' and self.synced():
            return None
        if part == 'meta' and self._metadata.synced():
            return None
        if part == 'both' and self.synced() and self._metadata.synced():
            return None

        try:
            cells = []

            if part in ('data', 'both'):
                cells += self.changes()
                self.cache.clear_changes()

            if part in ('meta', 'both'):
                cells += self._metadata.changes()

            return self.sheet.update_cells(cells)
        except gspread.exceptions.APIError as e:
            print(e)

    def reload(self):
        """
        Re-read data from google sheets,
        will clear out all unflushed changes, use with care
        """

        self._metadata.reload()
        self.cache.flush()
        self.prefetch(int(self._metadata['size']))

# Method group: end
