from functools import reduce
import gspread

class GSCellCache:
    """
    Class for caching local operations to later batch update,
        currently with no race condition protection in mind :P
    """
    __slots__ = ['_data', '_changes']

    def __init__(self):
        self._data = {}
        self._changes = {}

    def changes(self):
        return list(self._changes.values())

    def preload(self, datas):
        """
        Batch update data without issue changes
        """
        for data in datas:
            if (data.row, data.col) not in self._data:
                self._data[(data.row, data.col)] = data

    def update(self, key, value):
        self._data[key] = value

    def clear_changes(self):
        self._changes.clear()

    def flush(self):
        self._changes.clear()
        self._data.clear()

    def __contains__(self, key):
        if isinstance(key, tuple):
            return key in self._data
        if isinstance(key, str):
            grid = gspread.utils.a1_range_to_grid_range(key)
            r_1, r_2, c_1, c_2 = [
                grid[k]
                for k in 'startRowIndex endRowIndex startColumnIndex endColumnIndex'.split(' ')
            ]
            cells = [
                (r+1, c+1) in self._data for r in range(r_1, r_2) for c in range(c_1, c_2)
            ]
            return cells and reduce(lambda c, i: c&i, cells)
        return False

    def __getitem__(self, key):
        if isinstance(key, tuple):
            return self._data[key]

        if isinstance(key, str):
            grid = gspread.utils.a1_range_to_grid_range(key)
            r_1, r_2, c_1, c_2 = [
                grid[k]
                for k in 'startRowIndex endRowIndex startColumnIndex endColumnIndex'.split(' ')
            ]
            return [
                self._data[(r+1, c+1)] for r in range(r_1, r_2) for c in range(c_1, c_2)
            ]

        raise KeyError(key)

    def __setitem__(self, key, value):
        if key not in self._data or self._data[key] != value:
            self._data[key] = value
            self._changes[key] = value
        return value

    def __delitem__(self, key):
        del self._data[key]
        self._changes[key] = ""
