from datetime import datetime
from .base import GSModelBase, GSModelAttrs
from .utils import helpers

class AttackHistoryMetadata(GSModelAttrs):
    _fields = GSModelAttrs._fields + ['today_pivot']

class AttackHistory(GSModelBase):
    _columns = ['discord_id', 'iteration', 'boss#', 'waitingtime', 'added_at']
    _metadata_class = AttackHistoryMetadata

    @property
    def today_pivot(self):
        return self._metadata['today_pivot']

    @today_pivot.setter
    def today_pivot(self, value):
        self._metadata['today_pivot'] = value

    def setup(self):
        super().setup()
        self.today_pivot = self.pivot

    def add(self, discord_id, iteration, bossi, waitingtime):
        return super().add([discord_id, iteration, bossi, waitingtime, datetime.now().isoformat()])

    def get_today(self):
        offset = helpers.a1_distance(self.pivot, self.today_pivot)
        return self.read_rows(self.size - offset, pivot=self.today_pivot)
