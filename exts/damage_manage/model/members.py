from datetime import datetime
from .base import GSModelBase

class Members(GSModelBase):
    _columns = ['discord_id', 'can_takeover', 'added_at']

    def add(self, uid):
        super().add([uid, 0, datetime.now().isoformat()])

    def remove(self, uid):
        idxes = self.find_all([('discord_id', str(uid))])
        if idxes:
            return self.remove_row(idxes[0])
        return None

    def get_user(self, uid):
        idxes = self.find_all([('discord_id', str(uid))])
        if idxes:
            return self.get_row(idxes[0])
        return None

    def permit_takeover(self, uid):
        idxes = self.find_all([('discord_id', str(uid))])
        if not idxes:
            return False
        return [cell.value for cell in self.update(idxes[0], 'can_takeover', 1)]
