from .base import GSModelBase

class BossSettings(GSModelBase):
    _columns = ['boss#', 'hp', 'limit']

    def change(self, bossi, field, value):
        idxes = self.find_all([('boss#', str(bossi))])
        if idxes:
            return self.update(idxes[0], field, value)
        return False

    def get(self, bossi, field):
        idxes = self.find_all([('boss#', str(bossi))])
        if idxes and field in self._columns:
            return self.read_col(field)[idxes[0]]
        return None
