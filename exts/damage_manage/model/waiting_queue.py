from datetime import datetime
from .base import GSModelBase

class WaitingQueue(GSModelBase):
    _columns = ['discord_id', 'iteration', 'boss#', 'is_residue', 'added_at']

    def add(self, discord_id, iteration, boss, is_residue=False):
        residue = '1' if is_residue else '0'
        return super().add([discord_id, iteration, boss, residue, datetime.now().isoformat()])

    def remove(self, discord_id, iteration, boss):
        idxes = self.find_all([
            ('discord_id', str(discord_id)),
            ('iteration', str(iteration)),
            ('boss#', str(boss))
        ])

        if idxes:
            return self.remove_row(idxes[0])
        return False

    def waiting_for(self, iteration, boss, discord_id=None):
        query = [('iteration', str(iteration)), ('boss#', str(boss))]
        if discord_id:
            query.append(('discord_id', str(discord_id)))
        idxes = self.find_all(query)
        return [self.get_row(idx) for idx in idxes]
