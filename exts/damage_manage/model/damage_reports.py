from datetime import datetime
from .base import GSModelBase

class DamageReports(GSModelBase):
    _columns = ['discord_id', 'iteration', 'boss#', 'damage', 'reported_at']

    def add(self, discord_id, iteration, bossi, damage):
        return super().add([discord_id, iteration, bossi, damage, datetime.now().isoformat()])

    def update_damage(self, discord_id, damage):
        idxes = self.find_all([('discord_id', str(discord_id))])
        if idxes:
            return self.update(idxes[0], 'damage', damage)
        return False

    def reported_for(self, iteration, bossi):
        idxes = self.find_all([('iteration', str(iteration)), ('boss#', str(bossi))])
        return [self.get_row(idx) for idx in idxes]
