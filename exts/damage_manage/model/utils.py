import gspread

class helpers:
    @staticmethod
    def grid_to_a1_range(r1, c1, r2, c2):
        return '{}:{}'.format(
            gspread.utils.rowcol_to_a1(r1, c1),
            gspread.utils.rowcol_to_a1(r2, c2)
        )

    @staticmethod
    def a1_distance(tag1, tag2):
        return list(
            map(
                lambda pair: pair[1] - pair[0],
                zip(
                    gspread.utils.a1_to_rowcol(tag1),
                    gspread.utils.a1_to_rowcol(tag2)
                )
            )
        )

    @staticmethod
    def a1_offset(pivot, row_off, col_off=0):
        row, col = gspread.utils.a1_to_rowcol(pivot)
        if row+row_off < 1 or col+col_off < 1:
            return None
        return gspread.utils.rowcol_to_a1(row+row_off, col+col_off)
