from .base import GSAttrs

class Metadata(GSAttrs):
    _fields = [
        'iteration',
        'boss_index',
        'boss_count',
        'state',
        'paused'
    ]

    def setup(self):
        super().setup()
        self['iteration'] = 1
        self['boss_index'] = 0
        self['boss_count'] = 5
        self['state'] = 'initial'
        self['paused'] = 0
        self.flush()
