# pylint: disable=unsubscriptable-object
import functools
from typing import List, Union
import asyncio
from discord import User, Message, Reaction
from discord.ext import commands
from ..guild import GuildRecord

class helpers:

    @staticmethod
    async def is_paused(ctx):
        if GuildRecord[ctx.guild.id].paused == '1':
            return True
        return False

    @staticmethod
    def match_state():
        pass

    @staticmethod
    async def is_registered(ctx):
        idxs = GuildRecord[ctx.guild.id]['members'].find_all([("discord_id", str(ctx.author.id))])

        if idxs:
            return True
        return False

    @staticmethod
    async def dialog(ctx: commands.Context,
                     msg: Message,
                     emojis: List[str],
                     *,
                     timeout=30.0) -> Union[str,None]:

        for emoji in emojis:
            await msg.add_reaction(emoji)

        def validate(reaction: Reaction, user: User):
            return (
                reaction.emoji in emojis and
                user == ctx.author and
                msg.id == reaction.message.id
            )

        try:
            reaction, _ = await ctx.bot.wait_for(
                'reaction_add',
                timeout=timeout,
                check=validate
            )
        except asyncio.TimeoutError:
            return None
        else:
            return reaction.emoji

class decors:

    @staticmethod
    def paused_only(f):
        @functools.wraps(f)
        async def wrapped(self, ctx, *args, **kwargs):
            if GuildRecord[ctx.guild.id].paused == '1':
                await f(self, ctx, *args, **kwargs)
            else:
                await ctx.send('先暫停，不然等等爆炸我不管喔')
        return wrapped

    @staticmethod
    def not_paused(f):
        """
            Check if flag paused in metadata is set to 1,
            you should use this after `record_exists`
        """

        @functools.wraps(f)
        async def wrapped(self, ctx: commands.Context, *args, **kwargs):
            if GuildRecord[ctx.guild.id].paused != '1':
                await f(self, ctx, *args, **kwargs)
            else:
                await ctx.send('出事了，等一下')
        return wrapped

    @staticmethod
    def match_state(*,
                    allowed=[],
                    disallowed=[],
                    failedmsg='在這個狀態下你想幹什什什什麼啊'
                    ):
        def inner(f):
            @functools.wraps(f)
            async def wrapped(self, ctx, *args, **kwargs):
                state = GuildRecord[ctx.guild.id]['metadata']['state']
                if ((allowed and state not in allowed) or
                        (disallowed and state in disallowed)):
                    if failedmsg:
                        await ctx.send(failedmsg)
                else:
                    await f(self, ctx, *args, *kwargs)

            return wrapped
        return inner

    @staticmethod
    def registered_only(f):
        @functools.wraps(f)
        async def wrapped(self, ctx, *args, **kwargs):
            idxs = GuildRecord[ctx.guild.id]['members'].find_all([("discord_id", str(ctx.author.id))])
            if len(idxs) > 1:
                await ctx.send('??? 你怎麽有好幾個')
            elif len(idxs) > 0:
                await f(self, ctx, *args, **kwargs)
            else:
                await ctx.send('我不記得有你這個人啊')
        return wrapped

    @staticmethod
    def record_exists(f):
        @functools.wraps(f)
        async def wrapped(self, ctx, *args, **kwargs):
            if ctx.guild.id in GuildRecord:
                await f(self, ctx, *args, **kwargs)
            else:
                await ctx.send('你們現在連個表都沒有喔')
        return wrapped
