# pylint: disable=unsubscriptable-object
from discord.ext import commands
from ..guild import GuildRecord
from ..misc import Boss
from .utils import decors, helpers

class Member(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    async def cog_before_invoke(self, ctx):
        ctx.record: GuildRecord = GuildRecord[ctx.guild.id] if ctx.guild.id in GuildRecord else None

    # call for action
    async def cog_after_invoke(self, ctx):
        if ctx.record is None:
            return
        if await helpers.is_paused(ctx):
            return

        pboss = ctx.record.pending_boss()
        if (ctx.record.queue_full_for(*pboss) and
                ctx.record.state == 'queueing'):

            ctx.record.state = 'reporting'
            waiting = ctx.record.current_waiting()
            ctx.record.next_boss()
            ctx.record.flush('metadata')

            i, bi = ctx.record.current_boss
            message = ' '.join(['<@!{}>'.format(member[0]) for member in waiting])
            await ctx.send('塔臺廣播，請排 ({}-{}) 的 {} 開打，謝謝。'.format(i, bi, message))


    @commands.command()
    @commands.guild_only()
    @decors.record_exists
    async def sheet_url(self, ctx: commands.Context, *_):
        await ctx.send(ctx.record.sheets.url)

    @commands.command()
    @commands.guild_only()
    @decors.record_exists
    async def status(self, ctx: commands.Context):
        if ctx.record.paused == '1':
            await ctx.send('暫停中')
            return

        if ctx.record.state == 'reporting':
            i, bi = ctx.record.current_boss
            reported = ctx.record.current_reported()
            limit = ctx.record.get_boss(bi, 'limit')
            await ctx.send('傷害回報中 {}-{}({}/{})'.format(i, bi, len(reported), limit))

        i, bi = ctx.record.pending_boss()
        limit = ctx.record.get_boss(bi, 'limit')
        cnt = len(ctx.record.current_waiting())
        await ctx.send('等待中 {}-{}({}/{})'.format(i, bi, cnt, limit))

    @commands.command()
    @commands.guild_only()
    @decors.record_exists
    @decors.not_paused
    async def register(self, ctx: commands.Context):
        uid = ctx.author.id
        async with ctx.typing():
            ids = ctx.record.get_member(uid)
            if not ids:
                ctx.record.add_member(uid)
                ctx.record.flush('members')
                await ctx.send('{0.author} 我記住你了'.format(ctx))
            else:
                await ctx.send('註冊過了')
        return

    @commands.command()
    @commands.guild_only()
    @decors.record_exists
    @decors.not_paused
    @decors.match_state(disallowed=['initial'], failedmsg='還不到排隊的時候，是不是忘了 `!master start` 了？')
    @decors.registered_only
    async def enq(self, ctx: commands.Context, bossi: int, user=None):
        async with ctx.typing():
            cboss = ctx.record.current_boss
            boss = Boss(cboss.iteration, bossi, int(ctx.record.boss_count))
            if not boss.is_valid():
                await ctx.send('你是要打異世界的王喔 !?')
                return

            if boss <= cboss:
                boss.iteration += 1

            if ctx.record.get_member(ctx.author.id)[1] == '1':
                attendee = (
                    ctx.author.id
                    if user is None
                    else await commands.UserConverter().convert(ctx, user)
                )
            else:
                attendee = ctx.author.id

            can_enqueue = True
            for _ in range(3):
                if (ctx.record.checkInQueue(attendee, boss.iteration, boss.boss) or
                    ctx.record.queue_full_for(boss.iteration, boss.boss) ):
                    boss.iteration += 1
                else:
                    break
            else:
                await ctx.send('接下來三輪的預約都滿了啦，晚點再來吧。')
                can_enqueue = False

            if can_enqueue:
                ctx.record.enqueue(attendee, boss.iteration, boss.boss)
                ctx.record.flush('waiting')
                limit = ctx.record.get_boss(boss.boss, 'limit')
                await ctx.send('幫你預約了 {}-{}({}/{})'.format(
                    boss.iteration,
                    boss.boss,
                    len(ctx.record.waiting_for(boss.iteration, boss.boss)),
                    limit
                ))

    @commands.command()
    @commands.guild_only()
    @decors.record_exists
    @decors.not_paused
    @decors.match_state(allowed=['reporting'], failedmsg='現在沒有要你回報東西啊')
    @decors.registered_only
    async def report(self, ctx, damage, *_):
        async with ctx.typing():
            i, bi = ctx.record.current_boss
            expected = ctx.record.get_boss(bi, 'limit')
            if ctx.record.checkInQueue(ctx.author.id, i, bi):
                if ctx.record.report(ctx.author.id, damage):
                    reported = ctx.record.current_reported()
                    ctx.record.flush('damage_reports')
                    await ctx.send('現在有 {} 個人回報了, 還剩 {} 個人'.format(
                        len(reported),
                        int(expected)-len(reported)
                    ))
                else:
                    await ctx.send('你已經回報過了')

            else:
                await ctx.send('有人在鬧，{} 沒排隊啊'.format(ctx.author))
                ctx.record.paused = 1
                ctx.record.flush('metadata')
                return

            if ctx.record.all_reported():
                preselected, selected, cumulated, boss_hp = ctx.record.compute_damage()
                if cumulated >= boss_hp:
                    i, bi = ctx.record.current_boss

                    mentions = ' '.join(['<@!{}>'.format(discord_id) for discord_id in selected])
                    await ctx.send('其他人讓開讓 {} 出刀'.format(mentions))

                    residue = selected[-1]
                    if residue and residue not in preselected:
                        wait_rec = ctx.record.waiting_for(i+1, bi, residue)
                        if wait_rec:
                            ctx.record['waiting'].remove(wait_rec[0][0], i+1, bi)
                        ctx.record.enqueue(residue, i+1, bi, is_residue=True)
                        ctx.record.flush('waiting')
                        await ctx.send('<@!{}> 請注意你有殘刀會補在 ({}-{})'.format(selected[-1], i+1, bi))

                    ctx.record.write_history(i, bi, selected)
                    ctx.record.flush('attack_history')
                    ctx.record.state = 'queueing'
                    ctx.record.flush('metadata')
                else:
                    await ctx.send('尷尬了，輸出不夠請手動解決 ({}/{})'.format(cumulated, boss_hp))
                    ctx.record.paused = 1
                    ctx.record.flush('metadata')
