# pylint: disable=unsubscriptable-object
import discord
from discord.ext import commands
from ..guild import GuildRecord
from .utils import decors, helpers

class Master(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_before_invoke(self, ctx):
        ctx.record = GuildRecord[ctx.guild.id] if ctx.guild.id in GuildRecord else None

    # call for action
    async def cog_after_invoke(self, ctx):
        if ctx.record is None:
            return
        if await helpers.is_paused(ctx):
            return
        if (ctx.command.name == 'master'
                and ctx.invoked_subcommand.name == 'pause'):
            return

        pboss = ctx.record.pending_boss()
        if (ctx.record.queue_full_for(*pboss) and
                ctx.record.state == 'queueing'):

            ctx.record.state = 'reporting'
            waiting = ctx.record.current_waiting()
            ctx.record.next_boss()
            ctx.record.flush('metadata')

            i, bi = ctx.record.current_boss
            message = ' '.join(['<@!{}>'.format(member[0]) for member in waiting])
            await ctx.send('塔臺廣播，請排 ({}-{}) 的 {} 開打，謝謝。'.format(i, bi, message))

    @commands.group()
    async def master(self, ctx):
        pass

    @master.command()
    @commands.guild_only()
    async def setup(self, ctx: commands.Context, sheet, *_):
        if not sheet.startswith('https://'):
            await ctx.send('只接受網址喔')
            return

        msg: discord.Message = None
        async with ctx.typing():
            if ctx.guild.id in GuildRecord:
                msg = await ctx.send(
                    'Already initialized, add 👌 to this to confirm, will expire after 30s'
                )
                emoji = await helpers.dialog(ctx, msg, ['👌'])

                if emoji is None:
                    await msg.edit(content='Do nothing...')
                    await msg.clear_reactions()
                    return

                await msg.edit(content='Ok, now reinitializing...')
            else:
                msg = await ctx.send('Initializing')

            GuildRecord.bind(ctx.guild.id, sheet)
            GuildRecord[ctx.guild.id].setup()
            GuildRecord[ctx.guild.id].flush('all')

            await msg.edit(content='Initialized')
            await msg.clear_reactions()

    @master.command()
    @commands.guild_only()
    @decors.record_exists
    async def pause(self, ctx):
        if ctx.record.paused == '0':
            ctx.record.paused = 1
            ctx.record.flush('metadata')
            await ctx.send('暫停了，想要做什麼快點做')
        else:
            await ctx.send('已經暫停了啊')

    @master.command()
    @commands.guild_only()
    @decors.record_exists
    async def resume(self, ctx):
        if ctx.record.paused == '1':
            ctx.record.paused = 0
            ctx.record.flush('metadata')
            await ctx.send('繼續運作')
        else:
            await ctx.send('現在不是暫停中啊')

    @master.command()
    @commands.guild_only()
    async def recover(self, ctx: commands.Context, sheet):

        if not sheet.startswith('https://'):
            await ctx.send('只接受網址喔')
            return

        msg: discord.Message = None

        if ctx.guild.id in GuildRecord:
            if ctx.record.paused == '1':
                await ctx.send('先暫停，不然等等爆炸我不管喔')
                return

            msg = await ctx.send(
                '已經有表在運作中了，如果沒問題的話請送個 👌 給我，限時 30 秒'
            )

            emoji = await helpers.dialog(ctx, msg, ['👌'])

            if emoji is None:
                await msg.edit(content='時間到，啥都不做')
                return

            await msg.edit(content='好噢，從 {} 找回過去中....'.format(sheet))
        else:
            msg = await ctx.send('好噢，從 {} 找回過去中....'.format(sheet))

        GuildRecord.bind(ctx.guild.id, sheet)
        GuildRecord[ctx.guild.id].load()
        await msg.edit(content='完成')

    @master.command()
    @commands.guild_only()
    @decors.record_exists
    @decors.paused_only
    async def refresh(self, ctx, table='all'):
        async with ctx.typing():
            if table not in GuildRecord[ctx.guild.id] and table != 'all':
                await ctx.send('Invalid table name: {}'.format(table))
                return

            msg = await ctx.send(
                'reloading {} table{}'.format(table, 's' if table == 'all' else '')
            )
            ctx.record.reload(table)
            await msg.edit(content="reload done")

    @master.command()
    @commands.guild_only()
    @decors.record_exists
    async def add_angel(self, ctx, user):
        uc = commands.UserConverter()
        u = await uc.convert(ctx, user)
        if ctx.record.member_allow_takeover(str(u.id)):
            ctx.record.flush('members')
            await ctx.send("給你一對翅膀 {}".format(user))
        else:
            await ctx.send('這個人不存在')

    @master.command()
    @commands.guild_only()
    @decors.record_exists
    @decors.match_state(allowed=['initial'], failedmsg="已經開始了啦，還不快回去打")
    async def start(self, ctx):
        ctx.record.state = 'queueing'
        ctx.record.flush('metadata')
        await ctx.send('開始了 大家上喔')
