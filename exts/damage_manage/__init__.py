__all__ = [
    'commands',
    'guild',
    'model',

    'setup',
    'cleanup'
]

from .commands import *

def setup(bot):
    bot.add_cog(Master(bot))
    bot.add_cog(Member(bot))

def cleanup(bot):
    bot.remove_cog(Master.qualified_name)
    bot.remove_cog(Member.qualified_name)
