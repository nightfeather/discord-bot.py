import functools
from datetime import datetime, timedelta
from discord.ext import commands

start_time = datetime.fromisoformat("2021-01-11T00:30:00")
stop_time = datetime.fromisoformat("2021-01-11T00:30:00")

def to_timetuple(delta):
    hours, remain = divmod(delta.seconds, 3600)
    minutes, seconds = divmod(remain, 60)
    return {
        "days": delta.days,
        "hours": hours,
        "minutes": minutes,
        "seconds": seconds
    }

class Mushoku(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def mushoku(self, ctx):
        offset = datetime.now() - start_time
        pending_episode = offset // timedelta(days=7) + 2
        waiting_time = timedelta(days=7) - offset % timedelta(days=7)
        timetuple = to_timetuple(waiting_time)

        msg = ""

        if pending_episode < 26:
            msg = f"距離無職轉生第 {pending_episode} 集，還有"
        elif pending_episode == 26:
            msg = "距離無職轉生最後一集，還有"
        else:
            msg = f"距離不知道在哪裡播的無職轉生第 {pending_episode} 集，還有"

        if timetuple["days"] > 0:
            msg += f" {timetuple['days']} 天"

        if timetuple["hours"] > 0:
            msg += f" {timetuple['hours']} 小時"

        if timetuple["minutes"] > 0:
            msg += f" {timetuple['minutes']} 分"

        if timetuple["seconds"] > 0:
            msg += f" {timetuple['seconds']} 秒"

        await ctx.send(msg+"。")

def setup(bot):
    bot.add_cog(Mushoku(bot))

def cleanup(bot):
    bot.remove_cog(Mushoku.qualified_name)
