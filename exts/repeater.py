
from discord.ext import commands

class Repeater(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.message_record = {}

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot or message.content.startswith(self.bot.command_prefix):
            return

        if message.channel.id in self.message_record:
            if not message.content: # handle case when this is a attachment only message
                del self.message_record[message.channel.id]
                return

            rec = self.message_record[message.channel.id]
            if message.content == rec['content']:
                rec['count'] += 1
                print('`{}` has beed repeated {} times.'.format(rec['content'], rec['count']))
                if rec['count'] >= 3:
                    await message.channel.send(rec['content'])
                    rec['count'] = 0
            else:
                rec['content'] = message.content
                rec['count'] = 1
        else:
            self.message_record[message.channel.id] = {'content': message.content, 'count': 1}

def setup(bot):
    bot.add_cog(Repeater(bot))

def cleanup(bot):
    bot.remove_cog(Repeater.qualified_name)
