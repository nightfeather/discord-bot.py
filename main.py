#!/usr/bin/env python

from discord.ext import commands
import os

bot = commands.Bot(command_prefix="!")

bot.load_extension('exts.mushoku')
bot.load_extension('exts.repeater')
bot.load_extension('exts.owner')
bot.load_extension('exts.info')
bot.load_extension('exts.damage_manage')

@bot.event
async def on_ready():
    print(bot.user, 'is ready.')

#@bot.event
#async def on_command_error(ctx, error):
#    if type(error) is commands.CommandNotFound:
#        await ctx.send("Invalid command: `{}`".format(ctx.message.content))
#    elif type(error) is commands.NotOwner:
#        await ctx.send("You're not my owner.")
#    elif type(error) is commands.PrivateMessageOnly:
#        await ctx.send("This command only works in direct message.")
#    else:
#        await ctx.send(str(type(error)))
#        await ctx.send("Looks like you've triggered some dark matter.")

@bot.command(aliases=['乒'])
async def ping(ctx):
    await ctx.send('乓')

if os.environ.get('DISCORD_TOKEN'):
    bot.run(os.environ.get('DISCORD_TOKEN'))
