# 野生的凱留 bot

## 可以幫你管公主連結出刀記錄的 discord bot (預定)

## 安裝

```shell
git clone https://github.com/Nightfeather/discord-bot.py.git
cd discord-bot.py
source venv/bin/activate
pip install -r requirements.txt
```

## 預備工作

### 到 discord 申請一個 bot
- https://canary.discordapp.com/developers/applications

### 到 google developer console 申請一個 token
- https://console.developers.google.com/apis/dashboard
- 啟用試算表的 api
- 把 token 下載回來

## 執行

```shell
cd discord-bot.py
source venv/bin/activate
DISCORD_TOKEN=<discord token> GOOGLE_CREDENTIAL_FILE=<到下載回來的 token 的路徑> python main.py
```

## TODO

* 真的要來做 information_schema 了
>> ~~這是做 db-like 的東西的宿命嗎~~
* 做報表輸出
  * 很遺憾的，這東西還是人要看
* 篩刀條件調整
  * 加入剩餘時間判斷
* 狀態變動觸發
  * 把回報完成的觸發放到 after_invoke
  * 或是整個搬到 timer 去
  * 或者以上皆是
* 考慮個 firebase
  * 這樣前面報表輸出器或許可以改成 firebase 上的靜態網頁 ~~亂加工作量~~
